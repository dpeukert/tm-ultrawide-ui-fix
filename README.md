# Trackmania Ultrawide UI Fix plugin for Openplanet

This [Openplanet](https://openplanet.dev/) plugin fixes the in-game UI of Trackmania 2020 for aspect ratios wider than 16:9 by moving the UI elements back to the edge of the screen. Elements that appear while not driving are not the focus of this plugin, but MRs for them are certainly welcome. A fix for the animated main screen is provided by the [Menu Background Chooser](https://openplanet.dev/plugin/menu-bg-chooser) plugin by [XertroV](https://openplanet.dev/u/XertroV).

## Supported UI elements
* Chat (`FrameChat`)
* Network problem icon (`QuadNetworkProblem`)
* Hotseat mode player times (`HotSeat_InGame`)
* Countdown (`Race_Countdown`)
* Messages in the top left (`Race_DisplayMessage`)
* Lap counter (`Race_LapsCounter`)
* Slide out medal banner (`Race_Record`)
* Records (`Race_Record`)
* Respawn & Give up prompts (`Race_RespawnHelper`)
* Spectator mode picker (`Race_SpectatorBase`)
* Checkpoint times (`Race_TimeGap`)
* Warmup information (`Race_WarmUp`)
* Small score table (`Rounds_SmallScoresTable`)
* Best & previous times (`TimeAttack_BestRaceViewer`)
* COTD
  * Qualification information (`COTDQualifications_QualificationsProgress`)
  * Qualification results (`COTDQualifications_Ranking`)
  * Knockout results (`Knockout_KnockoutInfo`)
* Royal
  * Recently finished players (`Royal_FinishFeed`)
  * Team ranking (`Royal_LiveRanking`)
  * Restart prompt (`Royal_RespawnHelper`)
  * Team results (`Royal_TeamScore`)
* Ranked
  * Player positions and points (`Teams_Matchmaking_LiveRanking`)

## Not supported
* Splitscreen mode ([issue #12](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues/12))

## How to use this plugin
* Trackmania 2020 with Openplanet installed is required
* Search for `Ultrawide UI Fix` in the Plugin Manager
* Alternatively, download the plugin from its [Openplanet plugin page](https://openplanet.dev/plugin/ultrawideuifix) and place it Openplanet's Plugins directory

## Source code
The source code of this plugin is available on [GitLab](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix) under the MIT license.

## Bugs & issues
If you run into any bugs or issues, report them in the [issue tracker](https://gitlab.com/dpeukert/tm-ultrawide-ui-fix/-/issues).

## Exported function for use by other plugins
If you're writing a plugin of your own and want to support ultrawide aspect ratios, you can retrieve the current UI shift value (the amount of units the UI element should be moved/expanded by) as a `float` from this plugin:

```toml
# info.toml
[meta]
version = '1.0.0'

[script]
optional_dependencies = ['UltrawideUIFix']
```

```angelscript
// main.as
void Render() {
#if DEPENDENCY_ULTRAWIDEUIFIX
	UI::Text(tostring(UltrawideUIFix::GetUiShift()));
#else
	UI::Text('Not ultrawide');
#endif
}
```

## Authors
* \>= 3.0.0 - Dan_VQ / Daniel Peukert
* < 3.0.0 - [Eierpflanze / Felix Meyer](https://github.com/entw-fm)
