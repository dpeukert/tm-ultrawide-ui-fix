namespace Utils {
	// Print a debug log statement if enabled in settings
	void Trace(const string &in string) {
		if (g_Debug == true) {
			trace(string);
		}
	}

	// Contains all data storage and retrieval related code
	namespace Data {
		// Used as a name for a flag storing the last recorded time since TM startup
		const string FLAG_TIME_SINCE_STARTUP = 'time_since_startup';

		// Used as a name for a flag storing the current version of our table schemas
		const string FLAG_SCHEMA_VERSION = 'schema_version';

		// The currently used version of our table schemas
		const int SCHEMA_VERSION = 1;

		// Used to store data retreived from the database
		class Element {
			// The value that we set the element to the last time it was modified
			float lastValue;

			// The shift value we shifted the element by the last time it was modified
			float shiftedBy;

			Element(const float &in lastValue, const float &in shiftedBy) {
				this.lastValue = lastValue;
				this.shiftedBy = shiftedBy;
			}
		}

		// Contains logic for data storage and retrieval
		class Storage {
			// Used to store our SQLite database instance
			SQLite::Database@ db = null;

			Storage(const string &in path) {
				// Check if our database file exists
				bool dbExists = IO::FileExists(IO::FromStorageFolder(path));

				// Instantiate our SQLite database
				@this.db = SQLite::Database(IO::FromStorageFolder(path));

				if (dbExists == false) {
					// We're working with a new database, create our tables
					Utils::Trace('Database file does not exist, creating');
					this.CreateTables();
				} else if (this.IsNewStartup() == true) {
					// We're working with an existing database and we have a new startup, clear the element data table
					Utils::Trace('New start detected, clearing element data table');
					this.Clear();
				}
			}

			// Create the required tables and their columns
			void CreateTables() {
				Utils::Trace('Creating database tables');

				// Build the table creation query
				SQLite::Statement@ tableCreationQuery = this.db.Prepare("""
					CREATE TABLE flags (
						id VARCHAR(32) PRIMARY KEY,
						value INTEGER NOT NULL
					);
					CREATE TABLE element_data (
						id VARCHAR(256) PRIMARY KEY,
						last_value REAL NOT NULL,
						shifted_by REAL NOT NULL
					);
				""");

				// Execute the table creation query
				tableCreationQuery.Execute();

				// Build the query for storing the database schema version
				SQLite::Statement@ schemaVersionQuery = this.db.Prepare('INSERT INTO flags (id, value) VALUES(?, ?);');
				schemaVersionQuery.Bind(1, Utils::Data::FLAG_SCHEMA_VERSION);
				schemaVersionQuery.Bind(2, Utils::Data::SCHEMA_VERSION);

				// Store the database schema version
				schemaVersionQuery.Execute();
			}

			// Check whether we have a new startup as compared to the information stored in the databae
			bool IsNewStartup() {
				// Build the query
				SQLite::Statement@ query = this.db.Prepare('SELECT value FROM flags WHERE id = ? LIMIT 1;');
				query.Bind(1, Utils::Data::FLAG_TIME_SINCE_STARTUP);

				// Execute the query
				bool queryResult = query.NextRow();

				// Check the value from the query result
				bool isNew = true;

				if (queryResult && query.GetColumnInt('value') < int(GetApp().TimeSinceInitMs)) {
					isNew = false;
				}

				return isNew;
			}

			// Clear all element data from the database
			void Clear() {
				// Build the query
				SQLite::Statement@ query = this.db.Prepare('DELETE FROM element_data;');

				// Execute the query
				query.Execute();
			}

			// Retrieve element data for the provided ID
			Utils::Data::Element@ Get(const string &in id) {
				// Build the query
				SQLite::Statement@ query = this.db.Prepare('SELECT last_value, shifted_by FROM element_data WHERE id = ? LIMIT 1;');
				query.Bind(1, id);

				// Execute the query
				bool queryResult = query.NextRow();

				// Return the query results
				Utils::Data::Element@ results = null;

				if (queryResult) {
					@results = Utils::Data::Element(query.GetColumnFloat('last_value'), query.GetColumnFloat('shifted_by'));
				}

				return @results;
			}

			// Store the provided element data for the provided ID
			void Set(const string &in id, const float &in lastValue, const float &in shiftedBy) {
				// Build the set query
				SQLite::Statement@ setQuery = this.db.Prepare('INSERT INTO element_data (id, last_value, shifted_by) VALUES(?, ?, ?) ON CONFLICT(id) DO UPDATE SET last_value = excluded.last_value, shifted_by = excluded.shifted_by;');
				setQuery.Bind(1, id);
				setQuery.Bind(2, lastValue);
				setQuery.Bind(3, shiftedBy);

				// Execute the set query
				setQuery.Execute();

				// Build the query for storing time since startup
				SQLite::Statement@ timeSinceStartupQuery = this.db.Prepare('INSERT INTO flags (id, value) VALUES(?, ?) ON CONFLICT(id) DO UPDATE SET value = excluded.value;');
				timeSinceStartupQuery.Bind(1, Utils::Data::FLAG_TIME_SINCE_STARTUP);
				timeSinceStartupQuery.Bind(2, GetApp().TimeSinceInitMs);

				// Store time since startup
				timeSinceStartupQuery.Execute();
			}
		}
	}
}
