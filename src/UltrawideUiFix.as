namespace UltrawideUIFix {
	// Used for matching the starting tag of each Manialink page, which contains the UI layer name
	const string MANIALINK_TAG = '\n<manialink name="';

	// Used to describe transformations that apply to UI layers
	const string TYPE_UI_LAYER = 'ui_layer';

	// Used to describe transformations that apply to overlay frames
	const string TYPE_OVERLAY_FRAME = 'overlay_frame';

	// Direction value used for controls that don't have a constant side of the screen
	const int MOVE_AUTO = 0;

	// Direction value used for controls that should be moved to the left side of the screen
	const int MOVE_LEFT = -1;

	// Direction value used for controls that should be moved to the right side of the screen
	const int MOVE_RIGHT = 1;

	// Contains logic for each transformation type
	namespace Transformations {
		// An interface that is shared between all transformations
		interface TransformationInterface {
			// Apply the transformation
			void Execute(CMwNod@ object, float uiShift);

			// Return a new shift value based on the provided element ID, current value, UI shift and move direction
			float GetShiftValue(const string &in elementId, const float &in currentValue, const float &in uiShift, const int &in moveDirection, const string &in elementDescription);
		}

		// Contains logic shared between all transformations
		abstract class BaseTransformation {
			// A string describing whether this transformation affects UI layers or overlay frames
			string transformationType;

			// An ID describing the element being transformed, used as an primary key in the database
			string elementId;

			// The name of the object this transformation should apply to
			string objectName;

			BaseTransformation(const string &in objectName) {
				this.objectName = objectName;
			}

			void Execute(CMwNod@ object, float uiShift) {
				return;
			}

			float GetShiftValue(const string &in elementId, const float &in currentValue, const float &in uiShift, const int &in moveDirection, const string &in elementDescription) {
				// Get data for our element
				Utils::Data::Element@ elementData = g_DataStorage.Get(elementId);

				// If our value was kept and the control has already been moved by our current shift value, we don't want to do anything
				if (@elementData != null && elementData.lastValue == currentValue && elementData.shiftedBy == uiShift) {
					return 0.0;
				}

				// By default, start shifting from the initial value
				float prevShift = 0.0;

				// If we have element data and our value was kept, that means the shift value has changed, get the previous shift value and use it
				if (@elementData != null) {
					if (elementData.lastValue == currentValue) {
						prevShift = elementData.shiftedBy;
					} else {
						Utils::Trace(elementDescription + ' modified by someone else, ignoring our previous shift value');
					}
				}

				// Calculate the shift
				int direction = moveDirection;
				if (direction == UltrawideUIFix::MOVE_AUTO) {
					direction = currentValue >= 0 ? UltrawideUIFix::MOVE_RIGHT : UltrawideUIFix::MOVE_LEFT;
				}

				return direction * (uiShift - prevShift);
			}
		}

		// Contains logic shared between all UI layer transformations
		abstract class BaseUILayerTransformation : BaseTransformation {
			BaseUILayerTransformation(const string &in objectName) {
				super(objectName);
				this.transformationType = UltrawideUIFix::TYPE_UI_LAYER;
			}

			CGameManialinkControl@ GetControl(CGameManialinkPage@ page, const string &in selector) {
				// Check whether we're working with a class or an ID
				string selectorType = selector.SubStr(0, 1);
				string selectorWithoutType = selector.SubStr(1);

				if (selectorType == '.') {
					// Get our control based on a class
					page.GetClassChildren(selectorWithoutType, page.MainFrame, true);

					if (page.GetClassChildren_Result.Length > 0) {
						return page.GetClassChildren_Result[0];
					}
				} else if (selectorType == '#') {
					// Get our control based on an ID
					return page.GetFirstChild(selectorWithoutType);
				}

				// We we're unable to find our control, this should not happen, throw a warning
				warn('Unable to find control ' + selector + ' in UI layer ' + this.objectName);
				return null;
			}
		}

		// Contains logic shared between all overlay frame transformations
		abstract class BaseOverlayTransformation : BaseTransformation {
			// An array of strings describing the path in the overlay frame tree for the object this transformation should apply to
			array<string> overlayFramePath;

			BaseOverlayTransformation(const string &in objectName, array<string> overlayFramePath) {
				super(objectName);
				this.transformationType = UltrawideUIFix::TYPE_OVERLAY_FRAME;
				this.overlayFramePath = overlayFramePath;
			}
		}

		// Contains logic for moving controls
		class MoveTransformation : TransformationInterface, BaseUILayerTransformation {
			// The selector of the control this transformation should apply to
			string controlSelector;

			// The direction in which the control should be moved in
			int moveDirection;

			MoveTransformation(const string &in objectName, const string &in controlSelector, const int &in moveDirection) {
				super(objectName);
				this.elementId = this.transformationType + '_MoveTransformation_' + objectName + '_' + controlSelector;
				this.controlSelector = controlSelector;
				this.moveDirection = moveDirection;
			}

			void Execute(CMwNod@ object, float uiShift) override {
				// Get our page
				CGameUILayer@ uiLayer = cast<CGameUILayer@>(object);
				CGameManialinkPage@ page = uiLayer.LocalPage;

				// Get the control we want to move
				CGameManialinkControl@ control = this.GetControl(@page, this.controlSelector);

				if (control is null) {
					return;
				}

				// Get our shift value
				float controlShift = this.GetShiftValue(this.elementId, control.PosnX, uiShift, this.moveDirection, 'Control ' + this.controlSelector + ' of UI layer ' + this.objectName);
				if (controlShift == 0.0) {
					return;
				}

				// Move the control
				control.PosnX += controlShift;
				print('Control ' + this.controlSelector + ' of UI layer ' + this.objectName + ' moved by ' + controlShift + ' units');
				Utils::Trace('Control ' + this.controlSelector + ' of UI layer ' + this.objectName + ' moved from X=' + (control.PosnX - controlShift) + ' to X=' + control.PosnX);

				// Store data for our control
				g_DataStorage.Set(this.elementId, control.PosnX, uiShift);
			}
		}

		// Contains logic for expanding controls
		class ExpandTransformation : TransformationInterface, BaseUILayerTransformation {
			// The selector of the control this transformation should apply to
			string controlSelector;

			ExpandTransformation(const string &in objectName, const string &in controlSelector) {
				super(objectName);
				this.elementId = this.transformationType + '_ExpandTransformation_' + objectName + '_' + controlSelector;
				this.controlSelector = controlSelector;
			}

			void Execute(CMwNod@ object, float uiShift) override {
				// Get our page
				CGameUILayer@ uiLayer = cast<CGameUILayer@>(object);
				CGameManialinkPage@ page = uiLayer.LocalPage;

				// Get the control we want to expand
				CGameManialinkControl@ control = this.GetControl(@page, this.controlSelector);

				if (control is null) {
					return;
				}

				// Get our shift value (with move direction set to right, as we want to keep the size positive)
				float controlShift = 2 * this.GetShiftValue(this.elementId, control.Size.x, uiShift, UltrawideUIFix::MOVE_RIGHT, 'Control ' + this.controlSelector + ' of UI layer ' + this.objectName);
				if (controlShift == 0.0) {
					return;
				}

				// Expand the control
				control.Size = vec2(control.Size.x + controlShift, control.Size.y);

				print('Control ' + this.controlSelector + ' of UI layer ' + this.objectName + ' expanded by ' + controlShift + ' units');
				Utils::Trace('Control ' + this.controlSelector + ' of UI layer ' + this.objectName + ' expanded from W=' + (control.Size.x - controlShift) + ' to W=' + control.Size.x);

				// Store data for our control
				g_DataStorage.Set(this.elementId, control.Size.x, uiShift);
			}
		}

		// Contains logic for moving animation coordinates
		class AnimationMoveTransformation : TransformationInterface, BaseUILayerTransformation {
			// The regex used to match the animation we're changing
			string animationRegex;

			// The string used to replace the animation we're changing
			string animationReplace;

			// The direction in which the animation coordinates should be moved in
			int moveDirection;

			AnimationMoveTransformation(const string &in objectName, const string &in animationRegex, const string &in animationReplace, const int &in moveDirection) {
				super(objectName);
				this.elementId = this.transformationType + '_AnimationMoveTransformation_' + Crypto::MD5(animationRegex);
				this.animationRegex = animationRegex;
				this.animationReplace = animationReplace;
				this.moveDirection = moveDirection;
			}

			void Execute(CMwNod@ object, float uiShift) override {
				// Get our UI layer
				CGameUILayer@ uiLayer = cast<CGameUILayer@>(object);

				// Look for matches of our animation regex in the given UI layer
				Regex::SearchAllResult@ animationRegex = Regex::SearchAll(uiLayer.ManialinkPageUtf8, this.animationRegex);

				for (uint animationRegexMatchIndex = 0; animationRegexMatchIndex < animationRegex.Length; ++animationRegexMatchIndex) {
					string[] animationRegexMatch = animationRegex[animationRegexMatchIndex];

					// Check if we have everything we need captured by the regex
					if (animationRegexMatch.Length == 3) {
						// Store the coordinates
						string oldX = animationRegexMatch[1];
						string oldY = animationRegexMatch[2];
						float oldXFloat = Text::ParseFloat(oldX);

						// Get our shift value
						float animationShift = this.GetShiftValue(this.elementId + '_' + animationRegexMatchIndex, oldXFloat, uiShift, this.moveDirection, 'Coordinates of an animation for control in UI layer ' + this.objectName);
						if (animationShift == 0.0) {
							return;
						}

						// Prepare our new position and search and replace strings
						float newXFloat = oldXFloat + animationShift;
						string newX = Text::Format('%.5f', newXFloat);

						string search = this.animationReplace.Replace('%X', oldX).Replace('%Y', oldY);
						string replace = this.animationReplace.Replace('%X', newX).Replace('%Y', oldY);

						// Replace the animations in the Manialink code
						uiLayer.ManialinkPageUtf8 = uiLayer.ManialinkPageUtf8.Replace(search, replace);

						print('Coordinates of an animation for control in UI layer ' + this.objectName + ' moved by ' + animationShift + ' units');
						Utils::Trace('Animation string "' + search + '" for control in UI layer ' + this.objectName + ' replaced with "' + replace + '"');

						// Store data for our animation
						g_DataStorage.Set(this.elementId + '_' + animationRegexMatchIndex, newXFloat, uiShift);
					}
				}
			}
		}

		// Contains logic for moving overlay frame
		class OverlayFrameMoveTransformation : TransformationInterface, BaseOverlayTransformation {
			// The direction in which the overlay control should be moved in
			int moveDirection;

			OverlayFrameMoveTransformation(const string &in objectName, array<string> overlayFramePath, const int &in moveDirection) {
				super(objectName, overlayFramePath);
				this.elementId = this.transformationType + '_OverlayFrameMoveTransformation_' + objectName + '_' + string::Join(overlayFramePath, '_');
				this.moveDirection = moveDirection;
			}

			void Execute(CMwNod@ object, float uiShift) override {
				// Get our parent frame
				CControlFrame@ parentFrame;
				CControlFrame@ frame = cast<CControlFrame@>(object);
				uint parentFrameChildIndex;

				// Get the frame we want to move by looping through each path part, looking for it and storing it in the original frame variable
				for (uint overlayFramePathIndex = 0; overlayFramePathIndex < this.overlayFramePath.Length; ++overlayFramePathIndex) {
					// Loop through each child and check if it is the one we want
					bool overlayFramePathPartFound = false;
					for (uint overlayFrameChildIndex = 0; overlayFrameChildIndex < frame.Childs.Length; ++overlayFrameChildIndex) {
						if (frame.Childs[overlayFrameChildIndex].IdName == this.overlayFramePath[overlayFramePathIndex]) {
							overlayFramePathPartFound = true;
							@parentFrame = @frame;
							@frame = cast<CControlFrame@>(frame.Childs[overlayFrameChildIndex]);
							parentFrameChildIndex = overlayFrameChildIndex;
							break;
						}
					}

					// Check if we found the path part
					if (overlayFramePathPartFound == false) {
						// We did not, this should not happen, throw a warning
						warn('Unable to find subframe ' + string::Join(this.overlayFramePath, '/') + ' in frame ' + this.objectName);
						return;
					}
				}

				// Get the current location
				iso4 oldLocation = parentFrame.ChildsRelativeLocations[parentFrameChildIndex];
				float oldTX = oldLocation.get_tx();

				// Get our shift value (inverted and divided by 100 as compared to UI layers)
				float frameShift = (-1 * this.GetShiftValue(this.elementId, oldTX, uiShift, this.moveDirection, 'Subframe ' + string::Join(this.overlayFramePath, '/') + ' in frame ' + this.objectName)) / 100;
				if (frameShift == 0.0) {
					return;
				}

				// Move the frame
				parentFrame.ChildsRelativeLocations[parentFrameChildIndex] = iso4(mat4(oldLocation) * mat4::Translate(vec3(frameShift, 0, 0)));
				parentFrame.Hide();
				parentFrame.Show();

				print('Subframe ' + string::Join(this.overlayFramePath, '/') + ' in frame ' + this.objectName + ' moved by ' + frameShift + ' units');
				Utils::Trace('Subframe ' + string::Join(this.overlayFramePath, '/') + ' in frame ' + this.objectName + ' moved from tx=' + oldTX + ' to tx=' + (oldTX + frameShift));

				// Store data for our control
				g_DataStorage.Set(this.elementId, oldTX + frameShift, uiShift);
			}
		}
	}

	// Contains logic to apply the provided transformations
	class Fixer {
		// An array of transformations that should be applied
		array<UltrawideUIFix::Transformations::BaseTransformation@> transformations;

		Fixer(array<UltrawideUIFix::Transformations::BaseTransformation@> transformations) {
			this.transformations = transformations;
		}

		// Whether our transformations should be applied during the next frame, used to make sure all data is available
		bool applyNextFrame = false;

		// Whether our transformations should be applied at a specific timestamp in the future
		uint applyAtTimestamp = 0;

		// The amount of UI layers that existed in the previous frame, used to make sure we only apply transformations when the UI changes
		uint previousUiLayerLength = 0;

		// The amount of overlay frames that existed in the previous frame, used to make sure we only apply transformations when the UI changes
		uint previousOverlayFrameLength = 0;

		// The map UID for the previous frame, used to make sure we only apply transformations when the UI changes
		string previousMapUid = '';

		// The UI shift for the previous frame, used to make sure we only apply transformations when the UI changes
		float previousUiShift = 0.0;

		// Get the amount of units to shift UI elements by (https://maniaplanet-community.gitbook.io/maniascript/manialink/manialinks#manialink-positioning-system)
		float uiShift {
			get {
				// Check if we're in splitscreen mode, if so, we don't want to shift
				if (GetApp().CurrentPlayground !is null && GetApp().CurrentPlayground.Players.Length > 0 && GetApp().CurrentPlayground.Players[0].User.IdName.StartsWith('*splitscreen_')) {
					return 0.0;
				}

				// If we're not, calculate the shift
				float h = Draw::GetHeight();

				if (h == 0) {
					return 0.0;
				} else {
					return Math::Max(0.0, (float(Draw::GetWidth()) / (h / 9)) * 10 - 160);
				}
			}
		}

		// Normalize UI layer buffer
		MwFastBuffer<CGameUILayer@> uiLayers {
			get const {
				CGameManiaAppPlayground@ playground = GetApp().Network.ClientManiaAppPlayground;

				// Check if UI layers are available
				if (playground !is null) {
					return playground.UILayers;
				} else {
					// UI layers are not available, use an empty buffer
					MwFastBuffer<CGameUILayer@> noUiLayers;
					return noUiLayers;
				}
			}
		};

		// Normalize overlay frame array
		MwFastArray<CControlBase@> overlayFrames {
			get const {
				// Check if overlay frames are available
				if (GetApp().CurrentPlayground !is null && GetApp().CurrentPlayground.Interface !is null && GetApp().CurrentPlayground.Interface.InterfaceRoot !is null) {
					return GetApp().CurrentPlayground.Interface.InterfaceRoot.Childs;
				} else {
					// Overlay frames are not available, use an empty array
					MwFastArray<CControlBase@> no_overlay_frames;
					return no_overlay_frames;
				}
			}
		};

		// Get the current map UID
		string mapUid {
			get const {
				if (GetApp().CurrentPlayground !is null && GetApp().RootMap !is null) {
					return GetApp().RootMap.MapInfo.MapUid;
				} else {
					return '';
				}
			}
		};

		// Apply our transformations
		void ApplyTransformations() {
			Utils::Trace('Applying transformations');

			// Loop through each UI layer and store it in a dictionary under its ID
			dictionary uiLayerDictionary = {};
			for (uint uiLayerIndex = 0; uiLayerIndex < this.uiLayers.Length; ++uiLayerIndex) {
				CGameUILayer@ uiLayer = this.uiLayers[uiLayerIndex];
				string uiLayerName;

				// Check if we have a manialink tag with a name attribute
				string manialinkSnippet = uiLayer.ManialinkPageUtf8.SubStr(0, Math::Min(uiLayer.ManialinkPageUtf8.Length, 100));

				if (manialinkSnippet.StartsWith(UltrawideUIFix::MANIALINK_TAG)) {
					// If we do have one, get the value of the attribute
					string[] manialinkSnippetParts = manialinkSnippet.Split('"');

					if (manialinkSnippetParts.Length > 1) {
						uiLayerName = manialinkSnippetParts[1];
					}
				}

				// If the UI layer has a specified name, store it in the dictionary
				if (uiLayerName != '') {
					uiLayerDictionary[uiLayerName] = @uiLayer;
				}
			}

			// Loop through each overlay frame and store it in a dictionary under its ID
			dictionary overlayFrameDictionary = {};
			for (uint overlayFrameIndex = 0; overlayFrameIndex < this.overlayFrames.Length; ++overlayFrameIndex) {
				CControlBase@ frame = this.overlayFrames[overlayFrameIndex];

				// If the frame has a specified name, store it in the dictionary
				if (frame.IdName != '') {
					overlayFrameDictionary[frame.IdName] = @frame;
				}
			}

			// For each transformation, check if we have an UI layer or an overlay frame that matches, if so, execute the transformation
			for (uint transformationIndex = 0; transformationIndex < this.transformations.Length; ++transformationIndex) {
				UltrawideUIFix::Transformations::BaseTransformation@ transformation = this.transformations[transformationIndex];

				if (transformation.transformationType == UltrawideUIFix::TYPE_UI_LAYER && uiLayerDictionary.Exists(transformation.objectName)) {
					transformation.Execute(cast<CGameUILayer@>(uiLayerDictionary[transformation.objectName]), this.uiShift);
				} else if (transformation.transformationType == UltrawideUIFix::TYPE_OVERLAY_FRAME && overlayFrameDictionary.Exists(transformation.objectName)) {
					transformation.Execute(cast<CControlFrame@>(overlayFrameDictionary[transformation.objectName]), this.uiShift);
				}
			}

			// Mark that we applied our transformations
			this.applyNextFrame = false;
		}

		// Check if the UI has changed since the last state
		bool HasUiChanged() {
			if (g_Debug) {
				if (this.uiLayers.Length != this.previousUiLayerLength) {
					trace('UI layer count changed from ' + this.previousUiLayerLength + ' to ' + this.uiLayers.Length);
				}

				if (this.overlayFrames.Length != this.previousOverlayFrameLength) {
					trace('Overlay frame count changed from ' + this.previousOverlayFrameLength + ' to ' + this.overlayFrames.Length);
				}

				if (this.previousUiShift != this.uiShift) {
					trace('UI shift changed from ' + this.previousUiShift + ' to ' + this.uiShift);
				}

				if (this.previousMapUid != this.mapUid) {
					trace('Map UID changed from ' + this.previousMapUid + ' to ' + this.mapUid);
				}
			}

			return this.uiLayers.Length != this.previousUiLayerLength || this.overlayFrames.Length != this.previousOverlayFrameLength || this.previousUiShift != this.uiShift || this.previousMapUid != this.mapUid;
		}

		// Schedule transformations either for the next frame or a certain amount of milliseconds in the future
		void ScheduleTransformations(const int &in milliseconds = 0) {
			if (milliseconds == 0) {
				Utils::Trace('UI change detected, scheduling transformations for next frame');
				this.applyNextFrame = true;
			} else {
				Utils::Trace('UI change detected, scheduling transformations in ' + milliseconds + ' milliseconds');
				this.applyAtTimestamp = Time::get_Now() + milliseconds;
			}
		}

		// Render function called every frame from the global Render function
		void Render() {
			// Check if we've scheduled applying at a specific timestamp, if so, change the apply flag and clear the timestamp
			if (this.applyAtTimestamp != 0 && Time::get_Now() > this.applyAtTimestamp) {
				this.applyNextFrame = true;
				this.applyAtTimestamp = 0;
			}

			// If we're scheduled, apply transformations
			if (this.applyNextFrame == true) {
				this.ApplyTransformations();
			}

			// Check if there were any UI changes
			if (this.HasUiChanged()) {
				// If so, schedule transformations for the next frame
				this.ScheduleTransformations();

				// As some UI changes apply in Manialink init functions, also schedule transformations one second in the future to catch them
				this.ScheduleTransformations(1000);
			}

			// Store state so we have something to compare to when we get to the next frame
			this.previousUiLayerLength = this.uiLayers.Length;
			this.previousOverlayFrameLength = this.overlayFrames.Length;
			this.previousUiShift = this.uiShift;
			this.previousMapUid = this.mapUid;
		}
	}
}
