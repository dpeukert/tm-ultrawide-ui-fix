namespace UltrawideUIFix {
	// Return the latest shift value for use by other plugins
	import float GetUiShift() from 'UltrawideUIFix';
}
