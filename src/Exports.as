namespace UltrawideUIFix {
	// Return the latest shift value for use by other plugins
	float GetUiShift() {
		if (g_Fixer !is null) {
			return g_Fixer.previousUiShift;
		} else {
			return 0.0;
		}
	}
}
