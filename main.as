// The global instance of our data storage class
Utils::Data::Storage@ g_DataStorage = null;

// The global instance of our logic class
UltrawideUIFix::Fixer@ g_Fixer = null;

// The global frame counter
uint g_FrameCounter = 0;

// Main entry point
void Main() {
	// Instantiate our data storage class
	@g_DataStorage = Utils::Data::Storage('db.sqlite');

	// Instantiate our transformations
	array<UltrawideUIFix::Transformations::BaseTransformation@> transformations = {
		// Chat, bottom left
		UltrawideUIFix::Transformations::OverlayFrameMoveTransformation('FrameInGameBase', array<string> = {'FrameChat'}, UltrawideUIFix::MOVE_LEFT),
		UltrawideUIFix::Transformations::OverlayFrameMoveTransformation('FrameInGameBase', array<string> = {'FrameChat', 'FrameChatUnfolded'}, UltrawideUIFix::MOVE_RIGHT),
		// Network problem icon, bottom right
		UltrawideUIFix::Transformations::OverlayFrameMoveTransformation('FrameInGameBase', array<string> = {'#0', 'QuadNetworkProblem'}, UltrawideUIFix::MOVE_RIGHT),
		// Hotseat mode player times, top right
		UltrawideUIFix::Transformations::MoveTransformation('UIModules_HotSeat_InGame', '#frame-hotseatplayers', UltrawideUIFix::MOVE_LEFT),
		// Countdown, right
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_Countdown', '#Race_Countdown', UltrawideUIFix::MOVE_RIGHT),
		// Messages, top left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_DisplayMessage', '#Race_DisplayMessage', UltrawideUIFix::MOVE_LEFT),
		// Lap counter, top right
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_LapsCounter', '#Race_LapsCounter', UltrawideUIFix::MOVE_RIGHT),
		// Slide out medal banner, top left
		UltrawideUIFix::Transformations::AnimationMoveTransformation('UIModule_Race_Record', 'AnimMgr\\.Add\\(Controls\\.Frame_Medal, "<a pos=\\\\"([0-9.-]+) ([0-9.-]+)\\\\"', 'AnimMgr.Add(Controls.Frame_Medal, "<a pos=\\"%X %Y\\"', UltrawideUIFix::MOVE_LEFT),
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_Record', '#frame-medal', UltrawideUIFix::MOVE_LEFT),
		UltrawideUIFix::Transformations::ExpandTransformation('UIModule_Race_Record', '#clip-medal-banner'),
		// Records, left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_Record', '#Race_Record', UltrawideUIFix::MOVE_LEFT),
		// Respawn & Give up prompts, right
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_RespawnHelper', '#Race_RespawnHelper', UltrawideUIFix::MOVE_RIGHT),
		// Spectator mode picker, bottom right
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_SpectatorBase', '#Race_SpectatorBase_Commands', UltrawideUIFix::MOVE_RIGHT),
		// Checkpoint times, varies between offline and online
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_TimeGap', '#Race_TimeGap', UltrawideUIFix::MOVE_AUTO),
		// Warmup information, right
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Race_WarmUp', '#Race_WarmUp', UltrawideUIFix::MOVE_RIGHT),
		// Small score table, left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Rounds_SmallScoresTable', '#Rounds_SmallScoresTable', UltrawideUIFix::MOVE_LEFT),
		// Best & previous times, right
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_TimeAttack_BestRaceViewer', '#Race_BestRaceViewer', UltrawideUIFix::MOVE_RIGHT),

		// COTD
		// Qualification information, left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_COTDQualifications_QualificationsProgress', '#COTDQualifications_QualificationsProgress', UltrawideUIFix::MOVE_LEFT),
		UltrawideUIFix::Transformations::ExpandTransformation('UIModule_COTDQualifications_QualificationsProgress',  '#frame-global'),
		// Qualification results, left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_COTDQualifications_Ranking', '#COTDQualifications_Ranking', UltrawideUIFix::MOVE_LEFT),
		UltrawideUIFix::Transformations::ExpandTransformation('UIModule_COTDQualifications_Ranking', '#frame-global'),
		// Knockout results, left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Knockout_KnockoutInfo', '#Knockout_KnockoutInfo', UltrawideUIFix::MOVE_LEFT),

		// Royal
		// top right, recently finished players
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Royal_FinishFeed', '#Royal_FinishFeed', UltrawideUIFix::MOVE_RIGHT),
		// top left, team ranking
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Royal_LiveRanking', '#Royal_LiveRanking', UltrawideUIFix::MOVE_LEFT),
		// right, Restart prompt
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Royal_RespawnHelper', '#Royal_RespawnHelper', UltrawideUIFix::MOVE_RIGHT),
		// top right, team results
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Royal_TeamScore', '#Royal_TeamScore', UltrawideUIFix::MOVE_RIGHT),

		// Ranked
		// Player positions and points, left
		UltrawideUIFix::Transformations::MoveTransformation('UIModule_Teams_Matchmaking_LiveRanking', '#Teams_Matchmaking_LiveRanking', UltrawideUIFix::MOVE_LEFT)
	};

	// Instantiate our logic class
	@g_Fixer = UltrawideUIFix::Fixer(transformations);
}

// Render function called every frame
void Render() {
	// Call the Render function of our fixer instance every 10th frame to make sure we don't check for UI changes too often
	if (g_Fixer !is null && ++g_FrameCounter >= 9) {
		g_Fixer.Render();
		g_FrameCounter = 0;
	}
}
